/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pragma.principal;

import co.com.pragma.pojo.CiudadItem;
import co.com.pragma.pojo.ClienteItem;
import co.com.pragma.pojo.RequestResponse;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author gustavo.rodriguez
 */
public class CiudadController {

    static AmazonDynamoDB CLIENT = AmazonDynamoDBClientBuilder.standard().build();
    static DynamoDB DYNAMODB = new DynamoDB(CLIENT);
    static String TABLE_NAME = "ciudades";
    static Regions REGION = Regions.US_WEST_2;

    public Object handlerGetById(CiudadItem o, Context context) {
        RequestResponse rr = new RequestResponse();
        try {
            DynamoDBMapper mapper = new DynamoDBMapper(CLIENT);
            ClienteItem clienteFind = mapper.load(ClienteItem.class, o.getId());
            if (clienteFind != null) {
                rr.setStatus(200);
                rr.setMessage("Item encontrado!");
                rr.setData(clienteFind);
            } else {
                rr.setStatus(404);
                rr.setMessage("No hay registros!");
            }
        } catch (Exception e) {
            rr.setStatus(500);
            rr.setMessage(e.getMessage());
        }
        return rr;
    }

    public Object handlerGetData(CiudadItem o, Context context) {
        RequestResponse rr = new RequestResponse();
        try {
            if (o.getId() > 0) {
                return handlerGetById(o, context);
            } else {
                DynamoDBMapper mapper = new DynamoDBMapper(CLIENT);
                List<CiudadItem> itemList = new ArrayList<>();

                DynamoDBScanExpression query = new DynamoDBScanExpression();
                boolean filtro = false;
                Map<String, AttributeValue> attrs = new HashMap<>();
                StringBuilder strB = new StringBuilder();
                //**********
                if (o.getNombre()!= null) {
                    if (!o.getNombre().equals("")) {
                        filtro = true;
                        attrs.put(":v1", new AttributeValue().withS(o.getNombre()));
                        strB.append("contains(identificacion, :v1)");
                    }
                }
                ///*******
                if (filtro) {
                    System.out.println(strB.toString());
                    query.withFilterExpression(strB.toString())
                            .withExpressionAttributeValues(attrs);
                }
                ///*******
                if (!filtro) {
                    itemList = mapper.scan(CiudadItem.class, query);
                } else {
                    itemList = mapper.parallelScan(CiudadItem.class, query, 3);
                }

                if (itemList.size() > 0) {
                    rr.setStatus(200);
                    rr.setMessage("Lista de items encontrados!");
                    rr.setData(itemList);
                } else {
                    rr.setStatus(404);
                    rr.setMessage("No hay registros!");
                }
            }
        } catch (Exception e) {
            rr.setStatus(500);
            rr.setMessage(e.getMessage());
        }
        return rr;
    }

    public Object handlerCreate(CiudadItem c, Context context) {
        RequestResponse rr = new RequestResponse();
        try {
            DynamoDBMapper mapper = new DynamoDBMapper(CLIENT);
            c.setId((new Date()).getTime());
            mapper.save(c);
            rr.setStatus(200);
            rr.setMessage("Registro creado con exito!");
        } catch (Exception e) {
            rr.setStatus(500);
            rr.setMessage(e.getMessage());
        }
        return rr;
    }

    public Object handlerUpdate(CiudadItem c, Context context) {
        RequestResponse rr = new RequestResponse();
        try {
            DynamoDBMapper mapper = new DynamoDBMapper(CLIENT);
            CiudadItem old = mapper.load(CiudadItem.class, c.getId());
            if (old != null) {
                old.setNombre(c.getNombre());
                mapper.save(old);
                rr.setStatus(200);
                rr.setMessage("Registro actualizado con exito!");
            } else {
                rr.setStatus(404);
                rr.setMessage("El registro con id - " + c.getId() + " no existe!");
            }
        } catch (Exception e) {
            rr.setStatus(500);
            rr.setMessage(e.getMessage());
        }
        return rr;
    }

    public Object handlerDelete(CiudadItem c, Context context) {
        RequestResponse rr = new RequestResponse();
        try {
            DynamoDBMapper mapper = new DynamoDBMapper(CLIENT);
            CiudadItem old = mapper.load(CiudadItem.class, c.getId());
            if (old != null) {
                mapper.delete(old);
                rr.setStatus(200);
                rr.setMessage("Registro eliminado con exito!");
            } else {
                rr.setStatus(404);
                rr.setMessage("El registro con id - " + c.getId() + " no existe!");
            }

        } catch (Exception e) {
            rr.setStatus(500);
            rr.setMessage(e.getMessage());
        }
        return rr;
    }

}
