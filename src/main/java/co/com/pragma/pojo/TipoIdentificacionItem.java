/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pragma.pojo;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

/**
 *
 * @author gustavo.rodriguez
 */
@DynamoDBTable(tableName="tipo_identificaciones")
public class TipoIdentificacionItem {
    private long id;
    private String nombre;

    public TipoIdentificacionItem() {
    }

    @DynamoDBHashKey(attributeName="id")
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @DynamoDBAttribute(attributeName="nombre") 
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
