/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.pragma.pojo;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import java.util.Date;

/**
 *
 * @author gustavo.rodriguez
 */
@DynamoDBTable(tableName="clientes")
public class ClienteItem {
    private long id;
    private long tipoIdentificacion;
    private String identificacion;
    private String nombres;
    private String apellidos;
    private Date fechaNacimiento;
    private long ciudadNacimiento;
    private String filtro;

    public ClienteItem() {
    }

    @DynamoDBHashKey(attributeName="id")
    public long getId() {
        return id;
    }public void setId(long id) {
        this.id = id;
    }

    @DynamoDBAttribute(attributeName="tipo_identificacion")  
    public long getTipoIdentificacion() {
        return tipoIdentificacion;
    }
    public void setTipoIdentificacion(long tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @DynamoDBAttribute(attributeName="identificacion")  
    public String getIdentificacion() {
        return identificacion;
    }
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
    
    @DynamoDBAttribute(attributeName="nombres")  
    public String getNombres() {
        return nombres;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    @DynamoDBAttribute(attributeName="apellidos")  
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @DynamoDBAttribute(attributeName="fecha_nacimiento")  
    public Date getFechaNacimiento() {
        return this.fechaNacimiento;
    }
    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    

    @DynamoDBAttribute(attributeName="ciudad_nacimiento")  
    public long getCiudadNacimiento() {
        return ciudadNacimiento;
    }
    public void setCiudadNacimiento(long ciudadNacimiento) {
        this.ciudadNacimiento = ciudadNacimiento;
    }

    public String getFiltro() {
        return filtro;
    }
    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }
}
